package com.valikbr.carRental.repo;

import com.valikbr.carRental.model.Delivery;
import org.springframework.data.repository.CrudRepository;

public interface DeliveryRepository extends CrudRepository<Delivery, Integer> {
}
