package com.valikbr.carRental.repo;

import com.valikbr.carRental.model.Crash;
import org.springframework.data.repository.CrudRepository;

public interface CrashRepository extends CrudRepository<Crash, Integer> {
}
