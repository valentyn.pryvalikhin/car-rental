package com.valikbr.carRental.repo;

import com.valikbr.carRental.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}
