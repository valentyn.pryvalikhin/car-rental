package com.valikbr.carRental.repo;

import com.valikbr.carRental.model.Car;
import org.springframework.data.repository.CrudRepository;

public interface CarRepository extends CrudRepository<Car, Integer> {
}
