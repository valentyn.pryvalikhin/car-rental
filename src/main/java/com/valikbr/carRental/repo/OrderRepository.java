package com.valikbr.carRental.repo;

import com.valikbr.carRental.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {
}
