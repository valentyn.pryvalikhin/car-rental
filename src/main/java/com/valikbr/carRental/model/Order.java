package com.valikbr.carRental.model;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "order_")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Delivery delivery;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Crash crash;
    private Date firstDay;
    private Date lastDay;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Car car;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private User user;
    private Date created;
    private Status status;
    private String description;
    private int bill;
    private boolean withDriver;

    public enum Status {
        CREATED, ACCEPTED, REJECTED, PAID, INPROGRESS, CLOSED
    }

    public Order() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Crash getCrash() {
        return crash;
    }

    public void setCrash(Crash crash) {
        this.crash = crash;
    }

    public Date getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(Date firstDay) {
        this.firstDay = firstDay;
    }

    public Date getLastDay() {
        return lastDay;
    }

    public void setLastDay(Date lastDay) {
        this.lastDay = lastDay;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBill() {
        return bill;
    }

    public void setBill(int bill) {
        this.bill = bill;
    }

    public boolean isWithDriver() {
        return withDriver;
    }

    public void setWithDriver(boolean withDriver) {
        this.withDriver = withDriver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                bill == order.bill &&
                withDriver == order.withDriver &&
                delivery.equals(order.delivery) &&
                Objects.equals(crash, order.crash) &&
                firstDay.equals(order.firstDay) &&
                lastDay.equals(order.lastDay) &&
                car.equals(order.car) &&
                user.equals(order.user) &&
                created.equals(order.created) &&
                status == order.status &&
                Objects.equals(description, order.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, delivery, crash, firstDay, lastDay, car, user, created, status, description, bill, withDriver);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", address=" + delivery +
                ", crash=" + crash +
                ", firstDay=" + firstDay +
                ", lastDay=" + lastDay +
                ", car=" + car +
                ", user=" + user +
                ", created=" + created +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", bill=" + bill +
                ", withDriver=" + withDriver +
                '}';
    }
}
