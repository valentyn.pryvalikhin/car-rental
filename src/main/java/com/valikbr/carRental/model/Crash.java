package com.valikbr.carRental.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Crash {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int damageSum;
    private String crashDescription;
    private boolean paid;

    public Crash() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDamageSum() {
        return damageSum;
    }

    public void setDamageSum(int damageSum) {
        this.damageSum = damageSum;
    }

    public String getCrashDescription() {
        return crashDescription;
    }

    public void setCrashDescription(String crashDescription) {
        this.crashDescription = crashDescription;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Crash crash = (Crash) o;
        return id == crash.id &&
                damageSum == crash.damageSum &&
                paid == crash.paid &&
                Objects.equals(crashDescription, crash.crashDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, damageSum, crashDescription, paid);
    }

    @Override
    public String toString() {
        return "Crash{" +
                "id=" + id +
                ", damageSum=" + damageSum +
                ", crashDescription='" + crashDescription + '\'' +
                ", paid=" + paid +
                '}';
    }
}
